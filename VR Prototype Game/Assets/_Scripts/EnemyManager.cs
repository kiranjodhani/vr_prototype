﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    // Start is called before the first frame update

    Transform Player;
    float MovementSpeed;
    public float Distance=999;
    float MinimunDistance = 2;
    void Start()
    {
        MovementSpeed = Random.Range(1f, 2f);
        Player = GameObject.Find("Main Camera").transform;
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.localPosition = Vector3.MoveTowards(transform.localPosition, Player.localPosition, MovementSpeed*Time.deltaTime);
        Distance = Vector3.Distance(Player.transform.localPosition, transform.localPosition);
        if(Distance<= MinimunDistance)
        {
            GameManager.Instance.OnPlayerDemaged();
            Destroy(gameObject);
        }
    }
}
