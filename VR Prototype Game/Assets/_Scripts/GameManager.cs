﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR;

public class GameManager : MonoBehaviour
{

    public static GameManager Instance;
    //[Header("***** PLAYER ********")]
    //public GameObject Bullet_prefab;
    //RaycastHit hit;
    //public Camera PlayerCam;

    [Header("***** UI ********")]
    public Text ScoreText;
    public int Score;
    public Image PlayerHealthBar;
    public GameObject GameOverScreen;
    public GameObject BloosSplash;


    [Header("***** ENEMY PREFAB AND SPAWN POINTS ********")]
    public GameObject EnemyPrefab;
    public GameObject[] SpawnPoints;
    private void Awake()
    {
        Instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        for(int i =0; i < 10;i++)
        {
            SpawnEnemy();
        }
    }

    //public void ShootEnemy()
    //{

    //    Ray ray = PlayerCam.ViewportPointToRay(new Vector3(0.5F, 0.5F, 0));

    //    if (Physics.Raycast(ray, out hit))
    //    {
    //        print(hit.transform.name);
    //        if (hit.transform.name== "Enemy")
    //            {
    //                Destroy(hit.transform.gameObject);
    //                OnEnemyDestroyed();
    //            }
    //    }

    //}

    private void Update()
    {
        //ShootEnemy();
    }

    void SpawnEnemy()
    {
        GameObject Enemy = Instantiate(EnemyPrefab) as GameObject;
        Enemy.name = "Enemy";
        int RandomSpawnPoints = Random.Range(0, SpawnPoints.Length - 1);
        Vector3 TempPosition = SpawnPoints[RandomSpawnPoints].transform.localPosition;
        TempPosition.y = Random.Range(TempPosition.y + 5, TempPosition.y - 5);
        Enemy.transform.localPosition = TempPosition;
        Enemy.SetActive(true);
    }

    //Game manager will increase score when player bullet hit enemy
    public void OnEnemyDestroyed()
    {
        Score++;
        ScoreText.text = "Score : " + Score.ToString();
        SpawnEnemy();

    }



    //When enemy attacks player
    public void OnPlayerDemaged()
    {
        PlayerHealthBar.fillAmount -= 0.01f;
        SpawnEnemy();
        if (PlayerHealthBar.fillAmount<=0)
        {
			PlayerHealthBar.fillAmount = 1;
		}
        StartCoroutine(ShowBloodScreen());
    }

    IEnumerator ShowBloodScreen()
    {
        BloosSplash.SetActive(true);
        yield return new WaitForSeconds(1);
        BloosSplash.SetActive(false);

    }

    //When player does not have any health. Game will be over
    void OnGameOver()
    {
        Time.timeScale = 0;
        GameOverScreen.SetActive(true);
    }

}
